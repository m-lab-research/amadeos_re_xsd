/* Participants and roles */
// Customer
participant Customer
// System
participant System
role Driver of System
role Stockman of System
role OnlineCustomer of System
role Salesman of System


/* Objects */
object InvoiceDetail
object OrderHeader
object StockItem
object Catalog
object OrderDetail
object InvoiceHeader
object Delivery


/* Object references */
// DataInputs - existing objects
objectReference ExistingCatalog_ references Catalog existing
objectReference ExistingStockItem_ references StockItem existing
// DataObjects, DataOutputs, and MessageFlows
objectReference Delivery_Received references Delivery[Received]
objectReference Delivery_ references Delivery
objectReference StockItem_Prepared references StockItem[Prepared]
objectReference OrderDetail_Checked references OrderDetail[Checked]
objectReference OrderHeader_Canceled references OrderHeader[Canceled]
objectReference OrderDetail_New references OrderDetail[New]
objectReference OrderHeader_Accepted references OrderHeader[Accepted]
objectReference InvoiceHeader_ references InvoiceHeader
objectReference OrderHeader_New references OrderHeader[New]
objectReference Delivery_New references Delivery[New]
objectReference InvoiceDetail_New references InvoiceDetail[New]
objectReference InvoiceHeader_InDelivery references InvoiceHeader[InDelivery]
objectReference InvoiceHeader_New references InvoiceHeader[New]
objectReference InvoiceHeader_Delivered references InvoiceHeader[Delivered]


/* Tasks */
task OrderDetailSpecification {
    actor: OnlineCustomer
    input {
        ExistingCatalog_ multiplicity 1
        OrderHeader_New multiplicity 1
    }
    output {
        OrderDetail_New multiplicity -1
    }
}

task PrepareStockItem {
    actor: Stockman
    input {
        ExistingStockItem_ multiplicity 1
        InvoiceDetail_New multiplicity 1
    }
    output {
        StockItem_Prepared multiplicity 1
    }
}

task ReceiveInvoice {
    actor: Driver
    input {
        InvoiceHeader_ multiplicity 1
    }
    output {
    }
}

task InitiateOrder {
    actor: OnlineCustomer
    input {
    }
    output {
        OrderHeader_New multiplicity 1
    }
}

task CancelOrder {
    actor: Salesman
    input {
        OrderHeader_New multiplicity 1
    }
    output {
        OrderHeader_Canceled multiplicity 1
    }
}

task PreparingInvoice {
    actor: Salesman
    input {
        OrderHeader_Accepted multiplicity 1
    }
    output {
        InvoiceHeader_New multiplicity 1
    }
}

task UpdateDocumentStatus {
    actor: Stockman
    input {
        InvoiceHeader_InDelivery multiplicity 1
    }
    output {
        InvoiceHeader_Delivered multiplicity 1
    }
}

task Packing {
    actor: Stockman
    input {
        StockItem_Prepared multiplicity -1
        InvoiceHeader_New multiplicity 1
    }
    output {
        Delivery_New multiplicity 1
    }
}

task Delivering {
    actor: Driver
    input {
    }
    output {
        Delivery_ multiplicity 1
    }
}

task ReceiveDocumentation {
    actor: Driver
    input {
        InvoiceHeader_New multiplicity 1
    }
    output {
        InvoiceHeader_InDelivery multiplicity 1
    }
}

task LoadDelivery {
    actor: Driver
    input {
        Delivery_New multiplicity 1
    }
    output {
        Delivery_Received multiplicity 1
    }
}

task CheckCustomer {
    actor: Salesman
    input {
    }
    output {
    }
}

task AcceptOrder {
    actor: Salesman
    input {
        OrderHeader_New multiplicity 1
    }
    output {
        OrderHeader_Accepted multiplicity 1
    }
}

task GenerateInvoiceDetail {
    actor: Salesman
    input {
        InvoiceHeader_New multiplicity 1
        OrderDetail_Checked multiplicity -1
    }
    output {
        InvoiceDetail_New multiplicity -1
    }
}

task CheckOrderDetail {
    actor: Salesman
    input {
        ExistingStockItem_ multiplicity 1
        OrderDetail_New multiplicity -1
    }
    output {
        OrderDetail_Checked multiplicity -1
    }
}

task SendMessage_InvoiceHeader {
    actor: Customer
    input {
    }
    output {
        InvoiceHeader_ multiplicity 1
    }
}

task ReceiveMessage_Delivery {
    actor: Customer
    input {
        Delivery_ multiplicity 1
    }
    output {
    }
}

